<div class="row">
            <div class=" offset-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <form id="formulario" name="contact-form">
                    <h1>Contacte con nosotros</h1>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input type="text" name="telefono" class="form-control" placeholder="Teléfono">
                    </div>
                    <textarea class="form-control" name="editor1" id="editor1"></textarea>
                    <button type="submit" id="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>
        </div>