<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mudanzas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
        crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Gugi|Montserrat" rel="stylesheet">
</head>

<body>
    <!-- Header -->
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light menuborde">
            <div class=" header col offset-md-1">
                <a class="navbar-brand" href="#">
                    <img src="img/logo_nelmj.png" alt="Logo empresa">
                </a>
            </div>

            <!-- Menu para el móvil -->
            <div class="row">
                <div class="col offset-sm-4 col offset-md-4 col offset-lg-4 col offset-xl-4">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
            <!-- Menu -->
            <div class="collapse navbar-collapse offset-sm-7 col offset-md-7 col offset-lg-7 col offset-xl-7" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item ">
                        <a class="nav-link phonemenu" href="#">Inicio
                            <img class="icons" src="img/homr.png" alt="">
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link phonemenu" href="#">Portes
                            <img class="icons" src="img/paquete.png" alt="">
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link phonemenu" href="#">Ayuda
                            <img class="icons" src="img/help.png" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </nav>