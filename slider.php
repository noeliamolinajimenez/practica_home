
<div class="row slider">
            <div class="offset-md-1 col-sm-4 col-md-4 col-xl-4 col-lg-4">
                <img class="img1" src="img/caja.jpg" alt="" style="width:100%">
            </div>

            <div class="col-md-5 col-sm-5 col-lg-5 col-xl-5">
                <h1>Servicio
                    <span class="hours"> 24H LOS 7 DIAS</span>
                </h1>
                <h1>TODO PARA SU MUDANZA</h1>
                <p>Nosotros nos ponemos a su completa disposición. </p>
                <p>Para cualquier tipo de portes, nos comprometemos a hacerlo lo mas eficiente y rápido posible, además cuenta
                    con un seguro personalizado por si hubiera algún tipo de fractura en el material durante el transporte.</p>
                <p>Confienos sus mudanzas, sus portes, sus entregas u recogidas, ya que contamos con una flota de mas de 150
                    automoviles repartidos en toda España, tanto para cargas pesadas como para pequeñas mudanzas. Confie
                    en
                    <strong>Todo para su Mudanza </strong>.</p>
            </div>
        </div>

        <div class="row">
            <div class=" text2 offset-md-3 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <select class="form-control form-control-lg">
                    <option>Seleccione su ciudad</option>
                    <option value='alava'>Álava</option>
                    <option value='albacete'>Albacete</option>
                    <option value='alicante'>Alicante/Alacant</option>
                    <option value='almeria'>Almería</option>
                    <option value='asturias'>Asturias</option>
                    <option value='avila'>Ávila</option>
                    <option value='badajoz'>Badajoz</option>
                    <option value='barcelona'>Barcelona</option>
                    <option value='burgos'>Burgos</option>
                    <option value='caceres'>Cáceres</option>
                    <option value='cadiz'>Cádiz</option>
                    <option value='cantabria'>Cantabria</option>
                    <option value='castellon'>Castellón/Castelló</option>
                    <option value='ceuta'>Ceuta</option>
                    <option value='ciudadreal'>Ciudad Real</option>
                    <option value='cordoba'>Córdoba</option>
                    <option value='cuenca'>Cuenca</option>
                    <option value='girona'>Girona</option>
                    <option value='laspalmas'>Las Palmas</option>
                    <option value='granada'>Granada</option>
                    <option value='guadalajara'>Guadalajara</option>
                    <option value='guipuzcoa'>Guipúzcoa</option>
                    <option value='huelva'>Huelva</option>
                    <option value='huesca'>Huesca</option>
                    <option value='illesbalears'>Illes Balears</option>
                    <option value='jaen'>Jaén</option>
                    <option value='acoruña'>A Coruña</option>
                    <option value='larioja'>La Rioja</option>
                    <option value='leon'>León</option>
                    <option value='lleida'>Lleida</option>
                    <option value='lugo'>Lugo</option>
                    <option value='madrid'>Madrid</option>
                    <option value='malaga'>Málaga</option>
                    <option value='melilla'>Melilla</option>
                    <option value='murcia'>Murcia</option>
                    <option value='navarra'>Navarra</option>
                    <option value='ourense'>Ourense</option>
                    <option value='palencia'>Palencia</option>
                    <option value='pontevedra'>Pontevedra</option>
                    <option value='salamanca'>Salamanca</option>
                    <option value='segovia'>Segovia</option>
                    <option value='sevilla'>Sevilla</option>
                    <option value='soria'>Soria</option>
                    <option value='tarragona'>Tarragona</option>
                    <option value='santacruztenerife'>Santa Cruz de Tenerife</option>
                    <option value='teruel'>Teruel</option>
                    <option value='toledo'>Toledo</option>
                    <option value='valencia'>Valencia/Valéncia</option>
                    <option value='valladolid'>Valladolid</option>
                    <option value='vizcaya'>Vizcaya</option>
                    <option value='zamora'>Zamora</option>
                    <option value='zaragoza'>Zaragoza</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class=" text3 offset-md-1 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                <center>
                    <h1 class="titulo2">El mejor recurso de mudanzas a tu servicio
                        <span class="hours">24h</span>
                    </h1>
                </center>
            </div>
        </div>
