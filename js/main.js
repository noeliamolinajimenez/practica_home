$(document).ready(function () {

    CKEDITOR.replace('editor1');

});

$(document).on("click", "#submit", function () {

    $("#formulario").validate({
        ignore: [],
        onkeyup: false,
        onfocusout: false,
        errorClass: "is-invalid",

        // Cogen con los name no con los id
        rules: {
            nombre: {
                required: true,
                minlength: 3,
            },
            email: {
                required: true,
                email: true,
            },

            telefono: {
                required: true,
                minlength: 9,
                maxlength: 9,
                number: true,
            }
        },

        messages: {
            nombre: {
                minlength: "No puede haber un nombre con menos de 3 carácteres",
                required: "El nombre es obligatorio"
            },
            email: {
                required: "Debe introducir su email",
                email: "Introduzca un email valido"
            },
            telefono: {
                required: "Introduzca un número de teléfono",
                minlength: "No puede haber un numero con menos de 9 cifras",
                maxlength: "No puede haber un numero con mas de 9 cifras",
                number: "Escriba un numero de teléfono valido"
            }
        }
    });

    if ($("#formulario").valid()) {

        event.preventDefault();

        var formData = new FormData($("#formulario")[0]);
        var ckeditor = CKEDITOR.instances.editor1.getData();

        formData.append("formulario", ckeditor);

        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        };

        $.mockjax({
            url : "conect.html",
            responseText : "BIEN ECHO",
            status : 500,
            responseTime : 750,
            responseText : "A text response from the server"
        });
        // ajax
        $.ajax({
            type: 'POST',
            url: 'conect.html',
            data: formData,
            processData: false,
            contentType: false,

            success: function (data) {
                console.log("la respuesta es ", data);
                alert(data);
            },

            error: function () {
                console.log("Error");
            }
        });
    };

});