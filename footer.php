<footer class="page-footer font-small pt-0">

<div class="container">
    <div class="footer row pt-2 mb-3 text-center d-flex justify-content-center">

        <div class=" col-md-2 mb-3">
            <h6 class="text-uppercase ">
                <a href="#!">Portes</a>
            </h6>
        </div>
        <div class="col-md-2 mb-3">
            <h6 class="text-uppercase">
                <a href="#!">Ayuda</a>
            </h6>
        </div>

        <div class="col-md-2 mb-3">
            <h6 class="text-uppercase">
                <a href="#!">Contacto</a>
            </h6>
        </div>

    </div>

    <hr class="rgba-white-light" style="margin: 0 15%;">

    <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">

        <div class="col-md-8 col-12 mt-5">
            <p style="line-height: 1.7rem">Si necesita especialistas en mudanzas, nosotros somos esa empresa. Tenemos diferentes oficinas físicas
                repartidas en toda España. Mayor compromiso y mejor precio en " Todo para su mudanza ".</p>

        </div>


    </div>
</div>

<div class="footer-copyright py-3 text-center">
    © 2018 Copyright:
    <strong>Noelia Molina Jimenez</strong>

</div>

</footer>
</body>
</html>